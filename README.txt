This module will provide a Braintree JS Payment Method that 
will use the Braintree Payments client-side encryption script
to alleviate some of your PCI responsibilities.

More information: 
https://www.braintreepayments.com/braintrust/braintree-js

Installation
=========================
1. Download and install Commerce Braintree module: 
    https://drupal.org/project/commerce_braintree
2. Create a new directory in /sites/[all|whatever]/modules/commerce_braintree 
    called "modules" (e.g. /sites/all/modules/commerce_braintree/modules)
3. Place the commerce_braintree_js module into this new directory, so you have:
    .../commerce_braintree/modules/braintree_js/commerce_braintree_js.module
4. Sign up for a Braintree account
5. Enter your Merchant ID, Merchant Account ID, Public/Private keys and 
    Client-side encryption key into your Payment method configuration.
